package com.example.ivan.javachatfirebase.model;

import java.io.Serializable;

/**
 * Created by Ivan on 2.8.2016..
 */
public class ChatRoom implements Serializable {
    String id, name, lastMessage;
    int unreadCount;

    public ChatRoom() {

    }

    public ChatRoom(String id, String name, String lastMessage, int unreadCount) {
        this.id = id;
        this.name = name;
        this.lastMessage = lastMessage;
        this.unreadCount = unreadCount;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastMessage() {
        return this.lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public int getUnreadCount() {
        return this.unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

}
