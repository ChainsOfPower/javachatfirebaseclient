package com.example.ivan.javachatfirebase.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.app.EndPoints;
import com.example.ivan.javachatfirebase.app.MyApplication;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private String TAG = RegisterActivity.class.getSimpleName();
    private EditText inputName, inputMail, inputPassword, inputRepeatPassword;
    private Button btnRegister;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MyApplication.getInstance().getPrefManager().getUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_login_toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        inputName = (EditText) findViewById(R.id.activity_register_name);
        inputMail = (EditText) findViewById(R.id.activity_register_mail);
        inputPassword = (EditText) findViewById(R.id.activity_register_password);
        inputRepeatPassword = (EditText) findViewById(R.id.activity_register_repeat_password);
        btnRegister = (Button) findViewById(R.id.activity_register_register);
        progressBar = (ProgressBar) findViewById(R.id.activity_register_progress);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }

    private void register() {
        if(!validateName())
        {
            Toast.makeText(getApplicationContext(), "Niste unijeli ime", Toast.LENGTH_LONG).show();
            return;
        }

        if(!validateEmail())
        {
            Toast.makeText(getApplicationContext(), "Niste unijeli email", Toast.LENGTH_LONG).show();
            return;
        }

        if(!validatePasswords()) {
            Toast.makeText(getApplicationContext(), "Lozinke se ne podudaraju ili su prazne", Toast.LENGTH_LONG).show();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        final String name = inputName.getText().toString().trim();
        final String email = inputMail.getText().toString().trim();
        final String password = inputPassword.getText().toString().trim();
        final String firebaseToken = FirebaseInstanceId.getInstance().getToken().toString();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.REGISTER, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if(obj.getBoolean("error") == false) {
                        Toast.makeText(getApplicationContext(), "Uspješno ste se registrirali!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getApplicationContext(), StartActivity.class));
                        finish();
                    } else {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), "Korisnik sa tim imenom već postoji", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    progressBar.setVisibility(View.INVISIBLE);
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("mail", email);
                params.put("password", password);
                params.put("token", firebaseToken);

                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private boolean validateName() {
        return !inputName.getText().toString().trim().isEmpty();
    }

    private boolean validateEmail() {
        String email = inputMail.getText().toString().trim();

        if(email.isEmpty() || !isValidEmail(email))
        {
            return false;
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validatePasswords() {
        String password1 = inputPassword.getText().toString().trim();
        String password2 = inputRepeatPassword.getText().toString().trim();

        if(password1.isEmpty() || password2.isEmpty() || !password1.equals(password2))
        {
            return false;
        }

        return true;
    }
}
