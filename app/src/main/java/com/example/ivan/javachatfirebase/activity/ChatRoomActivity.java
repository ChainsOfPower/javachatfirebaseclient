package com.example.ivan.javachatfirebase.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.adapter.ChatRoomMessagesAdapter;
import com.example.ivan.javachatfirebase.app.Config;
import com.example.ivan.javachatfirebase.app.EndPoints;
import com.example.ivan.javachatfirebase.app.MyApplication;
import com.example.ivan.javachatfirebase.model.Message;
import com.example.ivan.javachatfirebase.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ChatRoomActivity extends AppCompatActivity {
    static final int ADD_NEW_USER_REQUEST = 2;
    private String TAG = ChatRoomActivity.class.getSimpleName();

    private String chatRoomId;
    private String chatRoomName;
    private ArrayList<Message> messageList;
    private RecyclerView recyclerView;
    private ChatRoomMessagesAdapter chatRoomMessagesAdapter;
    private EditText editTxtMessage;
    private Button btnSend;
    private ProgressBar progressBar;
    private BroadcastReceiver mbroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MyApplication.getInstance().getPrefManager().getUser() == null) {
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        setContentView(R.layout.activity_chat_room);

        editTxtMessage = (EditText) findViewById(R.id.activity_chat_room_message);
        btnSend = (Button) findViewById(R.id.activity_chat_room_send);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_chat_room_toolbar);
        setSupportActionBar(toolbar);

        if(savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                chatRoomId = null;
                chatRoomName = null;
            } else {
                chatRoomId = extras.getString("CHAT_ROOM_ID");
                chatRoomName = extras.getString("CHAT_ROOM_NAME");
            }
        }

        if(chatRoomId == null) {
            Toast.makeText(getApplicationContext(), "Soba nije pronađena!", Toast.LENGTH_SHORT).show();
            finish();
        }

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(chatRoomName);

        recyclerView = (RecyclerView) findViewById(R.id.activity_chat_room_recycler_view);
        messageList = new ArrayList<>();

        String selfUserId = MyApplication.getInstance().getPrefManager().getUser().getId();
        chatRoomMessagesAdapter = new ChatRoomMessagesAdapter(messageList, selfUserId);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(chatRoomMessagesAdapter);

        progressBar = (ProgressBar) findViewById(R.id.activity_chat_room_progress);
        /*progressBar.setVisibility(View.VISIBLE);

        fetchMessages();*/

        editTxtMessage.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                if (chatRoomMessagesAdapter.getItemCount() > 1) {
                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, chatRoomMessagesAdapter.getItemCount() - 1);
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        MyApplication.getInstance().getPrefManager().resetChatRoomUnreadCount(chatRoomId);

        mbroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "onReceive");

                if(intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    handlePushNotification(intent);
                }
            }
        };

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat_room, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_chat_room_add_person:
                getUser();
                return true;
            case R.id.menu_chat_room_logout:
                MyApplication.getInstance().logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getUser() {
        Intent addUser = new Intent(this, AddUserActivity.class);
        addUser.putExtra("CHAT_ROOM_ID", chatRoomId);
        startActivityForResult(addUser, ADD_NEW_USER_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == ADD_NEW_USER_REQUEST) {

            if(resultCode == Activity.RESULT_OK) {
                String strDate = getCurrentTime();
                Message myMessage = new Message();
                myMessage.setUser(MyApplication.getInstance().getPrefManager().getUser());
                myMessage.setCreatedAt(strDate);
                myMessage.setId("0");
                myMessage.setMessage("Hey, welcome to " + chatRoomName + "!");

                messageList.add(myMessage);
                chatRoomMessagesAdapter.notifyDataSetChanged();
                if (chatRoomMessagesAdapter.getItemCount() > 1) {
                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, chatRoomMessagesAdapter.getItemCount() - 1);
                }
                Log.e(TAG, "DODANA MOJA PORUKA");

                String userId = data.getStringExtra("id");
                String helloMessage = "Hello!";

                sendMessageToServer(userId, helloMessage);

                sendMessageToServer(MyApplication.getInstance().getPrefManager().getUser().getId(), myMessage.getMessage());
            }
        }
    }

    private void fetchMessages() {
        final String name = MyApplication.getInstance().getPrefManager().getUser().getName();
        final String password = MyApplication.getInstance().getPrefManager().getUser().getPlainPassword();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.CHAT_ROOM_MESSAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getBoolean("error") == false) {
                        JSONArray commentsObj = obj.getJSONArray("messages");

                        for (int i = 0; i < commentsObj.length(); i++) {
                            JSONObject commentObj = (JSONObject) commentsObj.get(i);

                            String commentId = commentObj.getString("id");
                            String commentText = commentObj.getString("message");
                            String createdAt = commentObj.getString("created_at");

                            //JSONObject userObj = commentObj.getJSONObject("user");
                            String userId = commentObj.getString("user_id");
                            String userName = commentObj.getString("name");
                            User user = new User(userId, userName, null, null);

                            Message message = new Message();
                            message.setId(commentId);
                            message.setMessage(commentText);
                            message.setCreatedAt(createdAt);
                            message.setUser(user);

                            messageList.add(message);
                        }

                        if (chatRoomMessagesAdapter.getItemCount() > 1) {
                            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, chatRoomMessagesAdapter.getItemCount() - 1);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Error prilikom dohvaćanja poruka!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }

                chatRoomMessagesAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("password", password);
                params.put("chat_room_id", chatRoomId);

                return params;
            }
        };
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private String getCurrentTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);

        return strDate;
    }

    private void sendMessage() {
        String messageToSend = editTxtMessage.getText().toString().trim();
        if(messageToSend.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Niste unjeli poruku!", Toast.LENGTH_SHORT).show();
            return;
        }

        String strDate = getCurrentTime();

        Message message = new Message("0", messageToSend, strDate, MyApplication.getInstance().getPrefManager().getUser());
        messageList.add(message);
        chatRoomMessagesAdapter.notifyDataSetChanged();
        if (chatRoomMessagesAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, chatRoomMessagesAdapter.getItemCount() - 1);
        }
        sendMessageToServer(MyApplication.getInstance().getPrefManager().getUser().getId(), messageToSend);
    }

    private void sendMessageToServer(final String idFrom, final String message) {
        final String name = MyApplication.getInstance().getPrefManager().getUser().getName();
        final String password = MyApplication.getInstance().getPrefManager().getUser().getPlainPassword();

        this.editTxtMessage.setText("");
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.SEND_MESSAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getBoolean("error") == false) {
                       /* JSONObject messageObj = obj.getJSONObject("message");
                        String messageId = messageObj.getString("id");
                        String messageText = messageObj.getString("message");
                        String createdAt = messageObj.getString("created_at");

                        JSONObject userObj = messageObj.getJSONObject("user");

                        String userId = userObj.getString("id");
                        String userName = userObj.getString("name");

                        User user = new User(userId, userName, null, null);

                        Message messageToSend = new Message();
                        messageToSend.setId(messageId);
                        messageToSend.setMessage(messageText);
                        messageToSend.setCreatedAt(createdAt);
                        messageToSend.setUser(user);

                        if (MyApplication.getInstance().getPrefManager().getUser().getId().equals(userId)) {
                            messageList.add(messageToSend);
                            chatRoomMessagesAdapter.notifyDataSetChanged();
                            if (chatRoomMessagesAdapter.getItemCount() > 1) {
                                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, chatRoomMessagesAdapter.getItemCount() - 1);
                            }
                        }*/
                    } else {
                        Toast.makeText(getApplicationContext(), "Greška prilikom slanja poruke", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("password", password);
                params.put("chat_room_id", chatRoomId);
                params.put("sender_id", idFrom);
                params.put("message", message);
                params.put("chat_room_name", chatRoomName);

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().getPrefManager().resetChatRoomUnreadCount(chatRoomId);

        LocalBroadcastManager.getInstance(this).registerReceiver(mbroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        progressBar.setVisibility(View.VISIBLE);
        messageList.clear();
        chatRoomMessagesAdapter.notifyDataSetChanged();
        fetchMessages();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mbroadcastReceiver);
        super.onPause();
    }

    private void handlePushNotification(Intent intent) {
        Log.e(TAG, "Unutar handlePushNotification");
        Message message = (Message) intent.getSerializableExtra("message");
        message.setCreatedAt(getCurrentTime());
        String chatRoomId = intent.getStringExtra("chat_room_id");
        String chatRoomName = intent.getStringExtra("chat_room_name");

        if(this.chatRoomId.equals(chatRoomId)) {
            MyApplication.getInstance().getPrefManager().resetChatRoomUnreadCount(chatRoomId);
            updateMessageRow(message);
        } else {
            Intent intentNotify = new Intent(getApplicationContext(), ChatRoomActivity.class);
            intentNotify.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentNotify.putExtra("CHAT_ROOM_ID", chatRoomId);
            intentNotify.putExtra("CHAT_ROOM_NAME", chatRoomName);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentNotify, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(chatRoomName)
                    .setContentText(message.getMessage())
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
        }
    }

    private void updateMessageRow(Message message) {
        messageList.add(message);
        chatRoomMessagesAdapter.notifyDataSetChanged();
        if (chatRoomMessagesAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, chatRoomMessagesAdapter.getItemCount() - 1);
        }
    }

}
