package com.example.ivan.javachatfirebase.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.app.MyApplication;

public class AddRoomActivity extends AppCompatActivity {
    private String TAG = AddRoomActivity.class.getSimpleName();

    private Button btnAddRoom, btnCancelAdding;
    private EditText inputName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MyApplication.getInstance().getPrefManager().getUser() == null) {
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        setContentView(R.layout.activity_add_room);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_add_room_toolbar);
        btnAddRoom  = (Button) findViewById(R.id.activity_add_room_add_button);
        btnCancelAdding = (Button) findViewById(R.id.activity_add_room_cancel_button);
        inputName = (EditText) findViewById(R.id.activity_add_room_room_name);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Nova soba");



        btnAddRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRoom();
            }
        });

        btnCancelAdding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelAdding();
            }
        });
    }

    private void addRoom() {
        String name = inputName.getText().toString().trim();

        if(name.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Unesite naziv sobe", Toast.LENGTH_LONG).show();
            return;
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("name", name);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void cancelAdding() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}
