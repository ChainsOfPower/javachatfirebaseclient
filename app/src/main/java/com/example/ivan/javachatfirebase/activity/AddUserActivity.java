package com.example.ivan.javachatfirebase.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.adapter.UsersAdapter;
import com.example.ivan.javachatfirebase.app.Config;
import com.example.ivan.javachatfirebase.app.EndPoints;
import com.example.ivan.javachatfirebase.app.MyApplication;
import com.example.ivan.javachatfirebase.helper.DividerItemDecoration;
import com.example.ivan.javachatfirebase.model.Message;
import com.example.ivan.javachatfirebase.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddUserActivity extends AppCompatActivity {
    private String TAG = AddUserActivity.class.getSimpleName();

    private String chatRoomId;

    private ArrayList<User> userList;
    private RecyclerView recyclerView;
    private UsersAdapter mAdapter;
    private ProgressBar progressBar;
    private Button btnSearchUsers, btnCancelAdding;
    private EditText inputName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MyApplication.getInstance().getPrefManager().getUser() == null) {
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        setContentView(R.layout.activity_add_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_add_user_toolbar);
        btnSearchUsers = (Button) findViewById(R.id.activity_add_user_btn_search);
        btnCancelAdding = (Button) findViewById(R.id.activity_add_user_cancel_button);
        inputName = (EditText) findViewById(R.id.activity_add_user_name);
        progressBar = (ProgressBar) findViewById(R.id.activity_add_user_progress);
        progressBar.setVisibility(View.GONE);

        userList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.activity_add_user_recycler_view);
        mAdapter = new UsersAdapter(userList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dodaj korisnika");

        if(savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                chatRoomId = null;
            } else {
                chatRoomId = extras.getString("CHAT_ROOM_ID");
            }
        }


        recyclerView.addOnItemTouchListener(new UsersAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new UsersAdapter.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                User user = userList.get(position);
                returnUser(user);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        btnSearchUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                searchUsers();
            }
        });

        btnCancelAdding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelAdding();
            }
        });


    }

    private void searchUsers() {
        final String name = MyApplication.getInstance().getPrefManager().getUser().getName();
        final String password = MyApplication.getInstance().getPrefManager().getUser().getPlainPassword();
        final String usernameToFind = inputName.getText().toString().trim();
        final String chatRoomIdToAddInto = chatRoomId;

        if(usernameToFind.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Niste unjeli ime korisnika", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
            return;
        }

        userList.clear();
        mAdapter.notifyDataSetChanged();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.FIND_USERS_BY_NAME_PART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getBoolean("error") == false) {
                        JSONArray usersArray = obj.getJSONArray("users");

                        if (usersArray.length() == 0) {
                            Toast.makeText(getApplicationContext(), "Ne postoji korisnik s unesenim imenom!", Toast.LENGTH_LONG).show();
                        }
                        for (int i = 0; i < usersArray.length(); i++) {
                            JSONObject userObj = (JSONObject) usersArray.get(i);
                            User user = new User();
                            user.setId(userObj.getString("id"));
                            user.setName(userObj.getString("name"));
                            user.setEmail(userObj.getString("email"));
                            user.setPlainPassword("");

                            userList.add(user);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Error prilikom dohvata podataka!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "json paring error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }
                mAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("password", password);
                params.put("user_name_to_find", usernameToFind);
                params.put("chat_room_id", chatRoomIdToAddInto);
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void returnUser(final User user) {
        new AlertDialog.Builder(this)
                .setTitle("Novi korisnik")
                .setMessage("Želite dodati " + user.getName() + "?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("id", user.getId());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void cancelAdding() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

}
