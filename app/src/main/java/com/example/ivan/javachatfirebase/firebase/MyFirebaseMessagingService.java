package com.example.ivan.javachatfirebase.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.activity.ChatRoomActivity;
import com.example.ivan.javachatfirebase.activity.MainActivity;
import com.example.ivan.javachatfirebase.app.Config;
import com.example.ivan.javachatfirebase.app.MyApplication;
import com.example.ivan.javachatfirebase.model.ChatRoom;
import com.example.ivan.javachatfirebase.model.Message;
import com.example.ivan.javachatfirebase.model.User;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ivan on 1.8.2016..
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if(MyApplication.getInstance().getPrefManager().getUser() == null) {
            Log.e(TAG, "user is not logged in, push notification ignored!");
            return;
        }
        JSONObject datObj = new JSONObject(remoteMessage.getData());
        String messageContent, chatRoomId, createdAt, messageId, chatRoomName;
        User user = new User();

        try {
            messageId = datObj.getString("message_id");
            messageContent = datObj.getString("message");
            createdAt = datObj.getString("created_at");
            chatRoomId = datObj.getString("chat_room_id");
            chatRoomName = datObj.getString("chat_room_name");

            user.setId(datObj.getString("user_id"));
            user.setName(datObj.getString("user_name"));
            user.setEmail(datObj.getString("user_email"));
            user.setPlainPassword("");

        }catch (JSONException e) {
            Log.e(TAG, "JSON parsing error");
            return;
        }
        Log.e(TAG, "from: " + user.getName());
        Log.e(TAG, "message id: " + messageId);
        Log.e(TAG, "message: " + messageContent);
        Log.e(TAG, "created at: " + createdAt);
        Log.e(TAG, "chat room id: " + chatRoomId);

        sendNotification(user,messageId,messageContent,createdAt,chatRoomId, chatRoomName);

    }

    private void sendNotification(User user, String messageId, String messageContent,String createdAt, String chatRoomId, String chatRoomName) {
        if(user.getName().equals(MyApplication.getInstance().getPrefManager().getUser().getName())) {
            Log.e(TAG, "Skipping self notification!");
            return;
        }
        MyApplication.getInstance().getPrefManager().increaseChatRoomUnreadCount(chatRoomId);

        Message message = new Message();
        message.setId(messageId);
        message.setUser(user);
        message.setMessage(messageContent);
        message.setCreatedAt(createdAt);

        if(!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            pushNotification.putExtra("chat_room_id", chatRoomId);
            pushNotification.putExtra("chat_room_name", chatRoomName);
            Log.e(TAG, "FFS: " + chatRoomId);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            NotificationUtils notificationUtils = new NotificationUtils();
            notificationUtils.playNotificationSound();
        } else {
            Intent intent = new Intent(getApplicationContext(), ChatRoomActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("CHAT_ROOM_ID", chatRoomId);
            intent.putExtra("CHAT_ROOM_NAME", chatRoomName);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(chatRoomName)
                    .setContentText(messageContent)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}
