package com.example.ivan.javachatfirebase.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.adapter.ChatRoomsAdapter;
import com.example.ivan.javachatfirebase.app.Config;
import com.example.ivan.javachatfirebase.app.EndPoints;
import com.example.ivan.javachatfirebase.app.MyApplication;
import com.example.ivan.javachatfirebase.helper.DividerItemDecoration;
import com.example.ivan.javachatfirebase.model.ChatRoom;
import com.example.ivan.javachatfirebase.model.Message;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    static final int ADD_NEW_ROOM_REQUEST = 1;
    private String TAG = MainActivity.class.getSimpleName();

    private ArrayList<ChatRoom> chatRoomList;
    private RecyclerView recyclerView;
    private ChatRoomsAdapter mAdapter;
    private ProgressBar progressBar;
    private BroadcastReceiver mbroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MyApplication.getInstance().getPrefManager().getUser() == null) {
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }


        setContentView(R.layout.activity_main);
        chatRoomList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.activity_main_recycler_view);

        progressBar = (ProgressBar) findViewById(R.id.activity_main_progress);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
        mAdapter = new ChatRoomsAdapter(chatRoomList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);


        recyclerView.addOnItemTouchListener(new ChatRoomsAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new ChatRoomsAdapter.ClickListener(){

            @Override
            public void onClick(View view, int position) {
                ChatRoom chatRoom = chatRoomList.get(position);

                chatRoom.setUnreadCount(0);
               /* chatRoomList.remove(position);
                chatRoomList.add(position, chatRoom); */
                mAdapter.notifyDataSetChanged();


                Intent intent = new Intent(MainActivity.this, ChatRoomActivity.class);
                intent.putExtra("CHAT_ROOM_ID", chatRoom.getId());
                intent.putExtra("CHAT_ROOM_NAME", chatRoom.getName());
                startActivity(intent);
                //Toast.makeText(getApplicationContext(), chatRoom.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
                //
            }
        }));



        mbroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "onReceive");
                if(intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    handlePushNotification(intent);
                }
            }
        };

        /*chatRoomList.clear();
        mAdapter.notifyDataSetChanged();
        fetchChatRooms();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_main_add_room:
                getRoomName();
                return true;
            case R.id.menu_main_logout:
                MyApplication.getInstance().logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fetchChatRooms() {
        final String name = MyApplication.getInstance().getPrefManager().getUser().getName();
        final String password = MyApplication.getInstance().getPrefManager().getUser().getPlainPassword();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.CHAT_ROOMS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getBoolean("error") == false) {
                        JSONArray chatRoomsArray = obj.getJSONArray("chat_rooms");

                        if(chatRoomsArray.length() == 0) {
                            Toast.makeText(getApplicationContext(), "Nemate nijedan aktivni razgovor!", Toast.LENGTH_LONG).show();
                        }
                        for(int i = 0; i < chatRoomsArray.length(); i++) {
                            JSONObject chatRoomObj = (JSONObject) chatRoomsArray.get(i);
                            ChatRoom cr = new ChatRoom();
                            cr.setId(chatRoomObj.getString("id"));
                            cr.setName(chatRoomObj.getString("name"));
                            cr.setLastMessage(chatRoomObj.getString("last_message"));
                            int unreadCount = MyApplication.getInstance().getPrefManager().getChatRoomUnreadCount(cr.getId());
                            cr.setUnreadCount(unreadCount);
                            Log.e("UNREAD COUNT", Integer.toString(unreadCount));
                            Log.e("UNREAD ID ACTIVITY: ",cr.getId());
                            chatRoomList.add(cr);
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Error prilikom dohvata podataka!", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }
                mAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("password", password);

                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void getRoomName() {
        Intent addRoom = new Intent(this, AddRoomActivity.class);
        startActivityForResult(addRoom, ADD_NEW_ROOM_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == ADD_NEW_ROOM_REQUEST) {

            if(resultCode == Activity.RESULT_OK) {
                String name = data.getStringExtra("name");
                addRoom(name);
            }
        }
    }

    private void addRoom(final String chatRoomName) {
        final String name = MyApplication.getInstance().getPrefManager().getUser().getName();
        final String password = MyApplication.getInstance().getPrefManager().getUser().getPlainPassword();

        //progressBarAddRoom.setVisibility(View.VISIBLE);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.ADD_CHAT_ROOM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getBoolean("error") == false) {

                        String chatRoomId = obj.getString("chat_room_id");
                        String chatRoomName = obj.getString("chat_room_name");
                        String lastMessage = obj.getString("message");

                        ChatRoom cr = new ChatRoom();
                        cr.setId(chatRoomId);
                        cr.setName(chatRoomName);
                        cr.setLastMessage(lastMessage);
                        cr.setUnreadCount(0);

                        chatRoomList.add(0, cr);
                    } else {
                        Toast.makeText(getApplicationContext(), "Greška prilikom kreiranja sobe!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() +  ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("password", password);
                params.put("chat_room_name", chatRoomName);

                Log.e(TAG, "paras: " + params.toString());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void handlePushNotification(Intent intent) {
        Log.e(TAG, "Unutar handlePushNotification");
        Message message = (Message) intent.getSerializableExtra("message");
        String chatRoomId = intent.getStringExtra("chat_room_id");
        String chatRoomName = intent.getStringExtra("chat_room_name");

        if(chatRoomId != null && message != null){
            updateRow(chatRoomId, chatRoomName, message);
        }
    }

    private void updateRow(String chatRoomId, String chatRoomName, Message message) {
        Log.e(TAG, "Unutar updateRow " + chatRoomId + " " + message);
        int index = 0;
        boolean roomExists = false;
        for(ChatRoom cr : chatRoomList) {
            if(cr.getId().equals(chatRoomId)) {
                index = chatRoomList.indexOf(cr);
                roomExists = true;
                //cr.setLastMessage(message.getMessage());
                //cr.setUnreadCount(cr.getUnreadCount() + 1);
                //chatRoomList.remove(index);
                //chatRoomList.add(0, cr);
                break;
            }
        }
        if(roomExists) {
            ChatRoom cr = chatRoomList.get(index);
            cr.setLastMessage(message.getMessage());
            cr.setUnreadCount(cr.getUnreadCount() + 1);
            chatRoomList.remove(index);
            chatRoomList.add(0, cr);
        } else {
            ChatRoom cr = new ChatRoom();
            cr.setId(chatRoomId);
            cr.setUnreadCount(1);
            cr.setLastMessage(message.getMessage());
            cr.setName(chatRoomName);
            chatRoomList.add(0, cr);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mbroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));


        progressBar.setVisibility(View.VISIBLE);
        chatRoomList.clear();
        mAdapter.notifyDataSetChanged();
        fetchChatRooms();

        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mbroadcastReceiver);
        super.onPause();
    }

}
