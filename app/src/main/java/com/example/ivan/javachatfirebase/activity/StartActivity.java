package com.example.ivan.javachatfirebase.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.app.MyApplication;
import com.google.firebase.iid.FirebaseInstanceId;

public class StartActivity extends AppCompatActivity {

    private String TAG = StartActivity.class.getSimpleName();

    private Button btnLogin;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Log.e(TAG, FirebaseInstanceId.getInstance().getToken());
        if(MyApplication.getInstance().getPrefManager().getUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.activity_start);

        btnLogin = (Button) findViewById(R.id.activity_start_login);
        btnRegister = (Button) findViewById(R.id.activity_start_register);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }

    private void register(){
        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
    }

    private void login() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }
}
