package com.example.ivan.javachatfirebase.model;

import java.io.Serializable;

/**
 * Created by Ivan on 2.8.2016..
 */
public class User implements Serializable {
    String id, name, email, plainPassword;

    public User() {

    }

    public User(String id, String name, String email, String plainPassword) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.plainPassword = plainPassword;
    }

    public String getId() {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPlainPassword() {return this.plainPassword;}

    public void setPlainPassword(String plainPassword) {this.plainPassword = plainPassword;}
}
