package com.example.ivan.javachatfirebase.firebase;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ivan.javachatfirebase.app.EndPoints;
import com.example.ivan.javachatfirebase.app.MyApplication;
import com.example.ivan.javachatfirebase.helper.MyPreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ivan on 1.8.2016..
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        if(MyApplication.getInstance().getPrefManager().getUser() != null) {
            final String name = MyApplication.getInstance().getPrefManager().getUser().getName();
            final String password = MyApplication.getInstance().getPrefManager().getUser().getPlainPassword();
            final String firebaseToken = FirebaseInstanceId.getInstance().getToken().toString();
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    EndPoints.LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "response: " + response);

                    try {
                        JSONObject obj = new JSONObject(response);

                        if (obj.getBoolean("error") == false) {
                            //Korisnikov token je uspješno updatean
                        } else {
                            Toast.makeText(getApplicationContext(), "UPDATE TOKENA NEUSPJEŠAN, LOGIRAJTE SE PONOVO", Toast.LENGTH_LONG).show();
                            MyApplication.getInstance().logout();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), "UPDATE TOKENA NEUSPJEŠAN, LOGIRAJTE SE PONOVO", Toast.LENGTH_LONG).show();
                        MyApplication.getInstance().logout();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "UPDATE TOKENA NEUSPJEŠAN, LOGIRAJTE SE PONOVO", Toast.LENGTH_LONG).show();
                    MyApplication.getInstance().logout();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", name);
                    params.put("password", password);
                    params.put("token", firebaseToken);

                    Log.e(TAG, "params: " + params.toString());
                    return params;
                }
            };

            MyApplication.getInstance().addToRequestQueue(strReq);
        }
    }
}
