package com.example.ivan.javachatfirebase.app;

/**
 * Created by Ivan on 2.8.2016..
 */
public class EndPoints {

    public static final String BASE_URL = "http://192.168.1.6:50000";
    public static final String REGISTER = BASE_URL + "/register";
    public static final String LOGIN = BASE_URL + "/login";
    public static final String CHAT_ROOMS = BASE_URL + "/get/rooms";
    public static final String ADD_CHAT_ROOM = BASE_URL + "/create/room";
    public static final String CHAT_ROOM_MESSAGES = BASE_URL + "/chat_room_messages";
    public static final String SEND_MESSAGE = BASE_URL + "/send_message";
    public static final String FIND_USERS_BY_NAME_PART = BASE_URL + "/find_user_by_name";
    public static final String LOGOUT = BASE_URL + "/logout";
}
