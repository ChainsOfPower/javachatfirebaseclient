package com.example.ivan.javachatfirebase.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.ivan.javachatfirebase.model.User;

/**
 * Created by Ivan on 2.8.2016..
 */
public class MyPreferenceManager {

    private String TAG = MyPreferenceManager.class.getSimpleName();

    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "java_chat_firebase";

    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_USER_PASSWORD = "user_password";
    private static final String KEY_NOTIFICATIONS = "notifications";

    public MyPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void storeUser(User user)
    {
        editor.putString(KEY_USER_ID, user.getId());
        editor.putString(KEY_USER_NAME, user.getName());
        editor.putString(KEY_USER_EMAIL, user.getEmail());
        editor.putString(KEY_USER_PASSWORD, user.getPlainPassword());
        editor.commit();

        Log.e(TAG, "User is stored in shared preferences. " + user.getName() + ", " + user.getEmail());

    }

    public User getUser()
    {
        if(pref.getString(KEY_USER_ID, null) != null) {
            String id, name, email, password;
            id = pref.getString(KEY_USER_ID, null);
            name = pref.getString(KEY_USER_NAME, null);
            email = pref.getString(KEY_USER_EMAIL, null);
            password = pref.getString(KEY_USER_PASSWORD, null);

            User user = new User(id, name, email, password);
            return user;
        }

        return null;
    }

    public void increaseChatRoomUnreadCount(String chatRoomId) {
        if(pref.getInt(chatRoomId, 0) == 0) {
            editor.putInt(chatRoomId, 1);
            editor.commit();
        } else {
            int current = pref.getInt(chatRoomId, 0);
            editor.putInt(chatRoomId, current + 1);
            editor.commit();
        }
    }

    public int getChatRoomUnreadCount(String chatRoomId) {
            return pref.getInt(chatRoomId, 0);
    }

    public void resetChatRoomUnreadCount(String chatRoomId) {
        editor.putInt(chatRoomId, 0);
        editor.commit();
    }


    public void addNotification(String notification) {
        String oldNotifications = getNotifications();

        if(oldNotifications != null)
        {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        editor.putString(KEY_NOTIFICATIONS, oldNotifications);
        editor.commit();
    }

    public String getNotifications() {
        return pref.getString(KEY_NOTIFICATIONS, null);
    }

    public void clear(){
        editor.clear();
        editor.commit();
    }
}
