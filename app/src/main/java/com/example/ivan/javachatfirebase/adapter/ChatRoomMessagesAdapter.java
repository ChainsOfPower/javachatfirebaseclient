package com.example.ivan.javachatfirebase.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ivan.javachatfirebase.R;
import com.example.ivan.javachatfirebase.model.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ivan on 10.8.2016..
 */
public class ChatRoomMessagesAdapter extends RecyclerView.Adapter<ChatRoomMessagesAdapter.MyViewHolder> {

    private String userId;
    private int SELF = 100;
    private int OTHER = 200;
    private static String today;
    private ArrayList<Message> messagesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView message, from, timestamp;

        public MyViewHolder(View view) {
            super(view);
            from = (TextView) itemView.findViewById(R.id.chat_item_from);
            message = (TextView) itemView.findViewById(R.id.chat_item_message);
            timestamp = (TextView) itemView.findViewById(R.id.chat_item_timestamp);
        }
    }

    public ChatRoomMessagesAdapter(ArrayList<Message> messagesList, String userId) {
        this.messagesList = messagesList;
        this.userId = userId;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int ViewType) {
        View itemView;

        if(ViewType == SELF) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_self, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_other, parent, false);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messagesList.get(position);
        if(message.getUser().getId().equals(userId)) {
            return SELF;
        }

        return OTHER;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Message message = messagesList.get(position);
        holder.from.setText(message.getUser().getName());

        holder.message.setText(message.getMessage());

        String timeStamp = getTimeStamp(message.getCreatedAt());
        Log.e("VRIJEME", timeStamp);
        holder.timestamp.setText(timeStamp);
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    public static String getTimeStamp(String dateStr) {
        if (dateStr.length() == 19) {

            return dateStr;
        }
        String timestamp = "";

        timestamp += dateStr.substring(0, 10);
        timestamp += " ";
        timestamp += dateStr.substring(11, 19);

        return timestamp;
    }


}
